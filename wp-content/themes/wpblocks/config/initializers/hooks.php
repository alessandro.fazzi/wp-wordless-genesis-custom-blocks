<?php

/*
 * Place here all your WordPress add_filter() and add_action() calls.
 */

add_filter('genesis_custom_blocks_template_path', 'wl_genesis_custom_blocks_path', 10, 2);
function wl_genesis_custom_blocks_path($path, $template_names) {
    return Wordless::theme_views_path();
}

add_filter('wordless_pug_configuration', 'prettier_pug');
function prettier_pug($ary) {
    $ary['pretty'] = true;
    return $ary;
}
