<?php

// This file will be loaded by phug CLI. it's passed as argument when invoked in `bin/watch_blocks`

// We need to load the whole WordPress in order to have all the possible functions
// while compiling blocks files. If we do not load it, we'd get fatal errors for
// undefined functions
require __DIR__ . '/../../../../wp-blog-header.php';
// And we need to load this file because it's not autoloaded by Wordless
require __DIR__.'/../../../plugins/wordless/wordless/helpers/pug/wordless_pug_options.php';

$templatesDirectories = [
    '../../themes/wpblocks/views/blocks',
];

// Overwrite `paths` options in order to restrict it to the blocks' folder while running the CLI
\Pug\Facade::setOptions(array_merge(WordlessPugOptions::get_options(), [
    'paths'     => $templatesDirectories,
]));
