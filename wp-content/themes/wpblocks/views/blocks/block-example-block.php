<h2><?= htmlspecialchars((is_bool($_pug_temp = block_field( 'title' )) ? var_export($_pug_temp, true) : $_pug_temp)) ?></h2>
<h3><?= htmlspecialchars((is_bool($_pug_temp = block_field( 'description' )) ? var_export($_pug_temp, true) : $_pug_temp)) ?></h3>
<p><strong>Scuro? -&gt; </strong><span><?= htmlspecialchars((is_bool($_pug_temp = block_field( 'dark' )) ? var_export($_pug_temp, true) : $_pug_temp)) ?></span></p>
